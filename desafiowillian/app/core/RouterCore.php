<?php

namespace app\core;

class Routercore
{
    private $uri;
    private $method;
    private $getArr = [];

    public function __construct()
    {
        $this->iniciarlizar();
        require_once('../app/config/Router.php');
        $this->executar();
    }

    private function iniciarlizar()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];

        if (strpos($uri, '?'))
            $uri = mb_substr($uri, 0, strpos($uri, '?'));

        $sepArray = explode('/', $uri);

        $uri = $this->corrigeArray($sepArray);

        for ($i = 0; $i < UNSET_URI_COUNT; $i++) {
            unset($uri[$i]);
        }

        $this->uri = implode('/', $this->corrigeArray($uri));
        if (DEBUG_URI) {
        }
    }

    private function corrigeArray($arr)
    {
        return array_values(array_filter($arr));
    }

    private function get($rota, $chamada)
    {
        $this->getArr[] = [
            'rota' => $rota,
            'chamada' => $chamada
        ];
    }

    private function post($rota, $chamada)
    {
        $this->getArr[] = [
            'rota' => $rota,
            'chamada' => $chamada
        ];
    }

    private function executar()
    {
    
        switch($this->method){
            case 'GET':
                $this->executarGet();
            break;
            case 'POST':
                $this->executarPost();
            break;
        }
    }

    private function executarGet()
    {
        foreach($this->getArr as $get){
            $rota = substr($get['rota'], 1);

            if($rota == $this->uri){
                $this->executarcontroller($get['chamada']);
            }
        }
    }

    private function executarPost()
    {
        foreach($this->getArr as $get){
            $rota = substr($get['rota'], 1);

            if($rota == $this->uri){
                $this->executarcontroller($get['chamada']);
            }
        }
    }

    private function executarcontroller($get)
    {
        $exp = explode('@', $get);
        $cont = 'app\\controller\\' . $exp[0];

        call_user_func_array([
            new $cont,
            $exp[1]
        ], []);
    }
}
