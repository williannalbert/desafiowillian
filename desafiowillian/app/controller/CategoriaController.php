<?php

namespace app\controller;

use app\classes\Input;
use app\models\CategoriaModel;

class CategoriaController
{
    private $categoriaModel;

    public function __construct ()
    {
        $this->categoriaModel = new CategoriaModel();
    }

    public function index()
    {
        $acao = $this->categoriaModel->getAll();
        dd($acao);
    }

    public function show()
    {
        $id = Input::get('id');
        $acao = $this->categoriaModel->getOne($id);
        dd($acao);
    }

    public function update()
    {
        $dados = $this->getInput();
        $acao = $this->categoriaModel->update($dados);
        dd("Categoria atualizada com sucesso.");
    }

    public function create()
    {
        $dados = $this->getInput();
        $acao = $this->categoriaModel->insert($dados);
        dd("cadastro realizado");

    }

    public function delete()
    {
        $id = Input::get('id');
        $acao = $this->categoriaModel->delete($id);  
        dd($acao);
    }

    private function getInput(){
        return(object)[
            'id' => Input::get('id'),
            'categoria' => Input::get('nome')
        ];
    }
}