<?php

namespace app\controller;

use app\classes\Input;
use app\models\ProdutoModel;

class ProdutoController
{
    private $produtoModel;

    public function __construct ()
    {
        $this->produtoModel = new ProdutoModel();
    }
    public function index()
    {
        $acao = $this->produtoModel->getAll();  
        dd($acao);
    }

    public function show()
    {
        $id = Input::get('id');
        $acao = $this->produtoModel->getOne($id);  
        dd($acao);
    }

    public function update()
    {
        $dados = $this->getInput();
        $acao = $this->produtoModel->update($dados);  
        dd("produto atualizado com sucesso");
    }
    public function create()    
    {
        $dados = $this->getInput();
        $cadastraProduto = $this->produtoModel->insert($dados);

        $cadastraProCat = $this->produtoModel->insertCategoria($cadastraProduto);

        dd("produto cadastrado");
    }

    public function delete()
    {
        $id = Input::get('id');
        $acao = $this->produtoModel->delete($id);  
        dd($acao);
    }

    private function getInput(){
        return(object)[
            'id' => Input::get('id')!="" ? Input::get('id') : "",
            'nome' => Input::get('nome'),
            'sku' => Input::get('sku'),
            'preco' => Input::get('preco'),
            'descricao' => Input::get('descricao'),
            'quantidade' => Input::get('quantidade'),
            'categoria' => Input::get('categoria')
        ];
    }
}