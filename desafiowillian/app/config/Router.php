<?php

$this->get('/categorias', 'CategoriaController@index');
$this->get('/categoria', 'CategoriaController@show');
$this->get('/delete_categoria', 'CategoriaController@delete');
$this->get('/produtos', 'ProdutoController@index');
$this->get('/produto', 'ProdutoController@show');
$this->get('/delete_produto', 'ProdutoController@delete');

$this->get('/create-produto', 'ProdutoController@create');
$this->get('/update-produto', 'ProdutoController@update');
$this->get('/create-categoria', 'CategoriaController@create');
$this->get('/update-categoria', 'CategoriaController@update');