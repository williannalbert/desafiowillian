<?php

namespace app\models;

use app\core\Model;

class ProdutoModel{
    private $model;

    public function __construct(){
        $this->model = new Model();

    }

    public function insert(object $parametros)
    {
        $sql = 'INSERT INTO produto (nome, sku, preco, descricao, quantidade) VALUES (:nome, :sku, :preco, :descricao, :quantidade)';
        $categorias = $parametros->categoria;
        $parametros = [
            ':nome' => $parametros->nome,
            ':sku' => $parametros->sku,
            ':preco' => $parametros->preco,
            ':descricao' => $parametros->descricao,
            ':quantidade' => $parametros->quantidade
        ];
        if(!$this->model->executeNonQuery($sql, $parametros))
            return -1;
        
        return [$this->model->getLastID(), $categorias];

    }

    public function insertCategoria($parametros)
    {
        
        $produto = $parametros[0];
        $categorias = explode(", ",$parametros[1]);
        $retornos = [];
        foreach($categorias as $cat){
            $sqlCat = 'INSERT INTO produtocategoria (produto_id, categoria_id) VALUES (:produto_id, :categoria_id)';
            $parametros = [
                ':produto_id' => $produto,
                ':categoria_id' => $cat
            ];
            
            if(!$this->model->executeNonQuery($sqlCat, $parametros))
                return -1;

            $retornos = $this->model->getLastID();
        }
        return $retornos;    
    }   
    
    public function update(object $parametros)
    {
        $sql = 'UPDATE produto SET nome = :nome,  sku = :sku, preco = :preco, descricao = :descricao, quantidade = :quantidade where id = :id' ;

        $parametros = [
            ':id' => $parametros->id,
            ':nome' => $parametros->nome,
            ':sku' => $parametros->sku,
            ':preco' => $parametros->preco,
            ':descricao' => $parametros->descricao,
            ':quantidade' => $parametros->quantidade
        ];

        return $this->model->executeNonQuery($sql, $parametros);
    }

    public function delete(Int $id)
    {
        $sql = 'DELETE FROM produto WHERE id = :id';
        $sqlProCat = 'DELETE FROM produtocategoria WHERE produto_id = :id';

        $parametros = [
            ':id' => $id
        ];
        $this->model->executeQueryOneRow($sqlProCat, $parametros);
        $this->model->executeQueryOneRow($sql, $parametros);
        

        dd("produto deletado");
    }

    public function getAll()
    {
        $sql = 'SELECT nome, sku, preco, descricao, quantidade FROM produto ORDER BY id';
        $dt = $this->model->executeQuery($sql);

        $listaProduto = null;
        
        foreach($dt as $dr){
            $listaProduto[] = $this->collection($dr);
        }

        return $listaProduto;
    }

    public function getOne(Int $id)
    {
        $sql = 'SELECT nome, sku, preco, descricao, quantidade FROM produto WHERE id = :id';

        $parametros = [
            ':id' => $id
        ];

        $dr = $this->model->executeQueryOneRow($sql, $parametros);

        return $this->collection($dr);
    }

    private function collection($parametros)
    {

        return (object)[
            ':nome' => $parametros['nome'],
            ':sku' => $parametros['sku'],
            ':preco' => $parametros['preco'],
            ':descricao' => $parametros['descricao'],
            ':quantidade' => $parametros['quantidade']
        ];
    }

}