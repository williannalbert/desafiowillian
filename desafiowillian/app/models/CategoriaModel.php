<?php

namespace app\models;

use app\core\Model;

class CategoriaModel{
    private $model;

    public function __construct(){
        $this->model = new Model();

    }

    public function insert(object $parametros)
    {
        $sql = 'INSERT INTO categoria (id, nome) VALUES (:id, :categoria)';

        $parametros = [
            ':id' => $parametros->id,
            ':categoria' => $parametros->categoria
        ];
        if(!$this->model->executeNonQuery($sql, $parametros))
            return -1;
        
        return $this->model->getLastID();
    }

    public function update(object $parametros)
    {
        $sql = 'UPDATE categoria SET nome = :categoria where id = :id';

        $parametros = [
            ':id' => $parametros->id,
            ':categoria' => $parametros->categoria
        ];

        return $this->model->executeNonQuery($sql, $parametros);
    }

    public function delete(Int $id)
    {
        $sqlCatProd = 'SELECT produto_id FROM produtocategoria WHERE categoria_id = :id';
        $sql = 'DELETE FROM categoria WHERE id = :id';

        $parametros = [
            ':id' => $id
        ];
        if(!$this->model->executeQueryOneRow($sqlCatProd, $parametros)== ""){
            dd("Categoria sendo utilizada em um produto.");    
        }
        $this->model->executeQueryOneRow($sql, $parametros);

        dd("categoria deletada");
    }

    public function getAll()
    {
        $sql = 'SELECT id, nome FROM categoria ORDER BY id';
        $dt = $this->model->executeQuery($sql);

        $listaCategoria = null;
        
        foreach($dt as $dr){
            $listaCategoria[] = $this->collection($dr);
        }

        return $listaCategoria;
    }

    public function getOne(Int $id)
    {
        $sql = 'SELECT id, nome FROM categoria WHERE id = :id';
   
        $parametros = [
            ':id' => $id
        ];

        $dr = $this->model->executeQueryOneRow($sql, $parametros);
        return $this->collectionOne($dr);
    }

    private function collection($parametros)
    {
        return (object)[
            ':id' => $parametros['id'],
            ':categoria' => $parametros['nome']
        ];
    }

    private function collectionOne($parametros)
    {
        return (object)[
            ':id' => $parametros['id'],
            ':nome' => $parametros['nome']
        ];
    }

}